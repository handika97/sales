import {
  SET_USER_CS,
  SET_IS_AUTH,
  RETRIEVE_REQ_LOAN,
  SET_SESSION_END
} from './actionTypes';

export const setUserCs = user => ({
  type: SET_USER_CS,
  payload: user
});

export const setIsAuth = boolean => ({
  type: SET_IS_AUTH,
  payload: boolean
})

export const retrieveReqLoan = data => ({
  type: RETRIEVE_REQ_LOAN,
  payload: data
})

export const setSessionEnd = data => ({
  type: SET_SESSION_END,
  payload: data
})