import { combineReducers } from 'redux';
import CSUser from './CSuser';
import Loan from './Loan';

export default combineReducers({
  CSLogin: CSUser,
  Loan: Loan
})
