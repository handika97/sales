import {
  RETRIEVE_REQ_LOAN
} from '../actions/actionTypes';

const initialState = {
  requested: {
    items: []
  },
  isLoading: true
};

export default (state = initialState, action) => {
  switch(action.type) {
    case RETRIEVE_REQ_LOAN:
      return {
        ...state,
        requested: action.payload.data,
        isLoading: false
      }
    default:
      return state;
  }
}