import {
  SET_USER_CS,
  SET_IS_AUTH,
  SET_SESSION_END
} from '../actions/actionTypes';

const initialState = {
  user: {},
  isAuthenticated: false,
  isSession: false,
  logoutCode: 0
}

export default function(state = initialState, action) {
  switch(action.type) {
    case SET_USER_CS:
      return {
        ...state,
        user: action.payload.data,
        isAuthenticated: false,
        logoutCode: 0,
        isSession: true
      }
    case SET_IS_AUTH:
      return {
        ...state,
        isAuthenticated: action.payload
      }
    case SET_SESSION_END:
      return {
        ...state,
        isSession: false,
        logoutCode: 2
      }
    default:
      return state;
  }
}