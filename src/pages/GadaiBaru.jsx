import React from 'react';
import styled from 'styled-components';

import { colors, sizes, device } from '../pages/Layout';

import Button from '../components/atoms/Button';

const GadaiBaru = () => {
  return (
    <>
      <Wrapper>
        <h1>Permohonan Gadai Baru</h1>
        <div className="steps">
          <div className="step done"><span>Isikan data</span></div>
          <div className="step"><span>Nasabah</span></div>
          <div className="step"><span>Barang</span></div>
        </div>
        <div className="search-form">
          <form onSubmit={e => e.preventDefault()}>
            <div className="form-group id-card">
              <div className="form-label">
                <label htmlFor="id-card">Jenis ID</label>
              </div>
              <div className="form-input">
                <select id="id-card">
                  <option value="1">-- Pilih Jenis ID --</option>
                  <option value="2">KTP</option>
                  <option value="3">Passport</option>
                </select>
              </div>
            </div>
            <div className="form-group id-number">
              <div className="form-label">
                <label htmlFor="id-number">Nomor Identitas</label>
              </div>
              <div className="form-input">
                <input id="id-number" type="text" placeholder="Nomor Identitas"/>
              </div>
            </div>
            <div className="form-group name">
              <div className="form-label">
                <label htmlFor="name">Nama</label>
              </div>
              <div className="form-input">
                <input id="name" type="text" placeholder="Nama"/>
              </div>
            </div>
            <div className="form-group phone">
              <div className="form-label">
                <label htmlFor="phone">No. HP</label>
              </div>
              <div className="form-input">
                <input id="phone" type="text" placeholder="No. HP"/>
              </div>
            </div>
            <Button primary>Cari</Button>
          </form>
        </div>
      </Wrapper>
    </>
  )
 
}

export default GadaiBaru;


const Wrapper = styled.div`
  @media ${device.tablet} {
    margin: 20px 0 ${sizes.sidebarMobile} 0;
    padding: 0 15px;
  }
  background-color: ${colors.white};
  margin: ${sizes.navbarHeight} 0 0 ${sizes.sidebarWidth};
  padding: 28px;
  h1 {
    @media ${device.tablet} {
      font-size: 22px;
    }
    font-size: 2rem;
    font-weight: bold;
    color: ${colors.primary};
  }
  .steps {
    height: 42px;
    display: inline-flex;
    margin-top: 3rem;
    @media ${device.tablet} {
      display: none;
    }
    .step {
      position: relative;
      min-width: 130px;
      display: flex;
      align-items: center;
      justify-content: center;
      background-color: #EBEBEB;
      color: ${colors.secondary};
      clip-path: polygon(90% 0, 100% 50%, 90% 100%, 0 100%, 0 0);
      font-size: 12px;
      &:first-child {
        z-index: 3;
      }
      &:nth-child(2) {
        z-index: 2;
      }
      &:last-child {
        z-index: 1;
      }
      &:not(:first-child) {
        margin-left: -18px;
      }
      &::after {
        background-color: white;
        content: '';
        position: absolute;
        top: 3px;
        bottom: 3px;
        left: 3px;
        right: 3px;
        z-index: -1;
        clip-path: polygon(90% 0, 100% 50%, 90% 100%, 0 100%, 0 0);
      }
      &.done {
        background-color: #EBEBEB;
        color: white;
        &::after {
          background-color: ${colors.secondary};
        }
      }
    }
  }
  .search-form {
    margin-top: 1rem;
    padding: 12px 16px;
    border-radius: 8px;
    box-shadow: 0 4px 14px rgba(0, 0, 0, .1);
    background-color: white;
    form {
      @media ${device.tablet} {
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;
        padding: 12px;
      }
      box-sizing: border-box;
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      .id-card {
        @media ${device.tablet} {
          order: 1;
        }
        order: 3;
      }
      .id-number {
        @media ${device.tablet} {
          order: 2;
        }
        order: 4;
      }
      .name {
        @media ${device.tablet} {
          order: 3;
        }
        order: 1;
      }
      .phone {
        @media ${device.tablet} {
          margin-right: 0;
          order: 4;
        }
        order: 2;
      }
      button {
        @media ${device.mobileL} {
          width: 100%;
          margin-left: 0;
        }
        @media ${device.tablet} {
          margin-top: 1rem;
        }
        margin-left: auto;
        order: 5;
        padding: 6px;
      }

      .form-group {
        margin-right: 1rem;
        @media ${device.tablet} {
          display: flex;
          justify-content: space-between;
          width: 100%;
          &:not(:first-child) {
            margin-top: .7rem;
          }
        }
        .form-label {
          @media ${device.tablet} {
            display: block;
            width: 30%;
          }
          display: none;
          label {
            font-size: 12px;
          }
        }
        .form-input {
          @media ${device.tablet} {
            width: 70%;
            padding-left: 20px;
          }
          input, select, option {
            @media ${device.tablet} {
              width: 100%;
            }
            width: 200px;
            padding: 6px 12px;
            border-radius: 4px;
            background-color: transparent;
            border: 1.5px solid rgba(0, 0, 0, .2);
            outline: none;
            transition: .1s;
            color: ${colors.black};
            &:focus {
              box-shadow: 0 0 2px 2.3px rgba(99, 43, 141, .2);
            }
          }
        }
      }
    }
  }
`;