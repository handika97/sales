import React, { Fragment, useEffect, useState } from "react";
import styled from "styled-components";
import { useHistory, useRouteMatch, useLocation } from "react-router-dom";

import { colors, sizes, device } from "./Layout";
import Modal from '../components/cells/Modal';
import Button from "../components/atoms/Button";
import TabContent from "../components/cells/TabContent";
import Navbar from "../components/cells/Navbar";
import Sidebar from "../components/cells/Sidebar";
import defaultProfile from "../assets/img/jpg/default_profile.jpg";
import Table from "../components/cells/Table";

const tabs = ['kelengkapan', 'aksesoris', 'kondisi']

const Menu =({title,content})=>{
  return(
    <div className="item-menu">
    <p className="title">{title}</p>
    <p>:</p>
    <p className="item-content">{content}</p>
    </div>
  )
}
export default () => {
  useEffect(() => {
    document.title = "Dashboard - Raja Gadai";
  }, []);

  const {search} = useLocation();
  const queryTab = search.split('=')[1] || tabs[0];

  const [showModalRincian, toggleModalRincian] = useState(false);

  return (
    <Fragment>
      <Modal
        showModal={showModalRincian}
        toggleModal={toggleModalRincian}
        headerLabel="ganti nama header disini"
        okeLabel="ganti nama button"
        cancelLabel="ganti nama button"
      >
      </Modal>
      <Navbar />
      <Sidebar />
      <ProfileMobile>
        <div className="wrapper">
          <div className="user-info">
            <h1>Welcome</h1>
            <h2>Nurhidayah</h2>
            <p>Customer Service - Cabang CIMONE</p>
          </div>
          <div className="user-image">
            <img src={defaultProfile} alt="default" />
          </div>
        </div>
      </ProfileMobile>
      <Wrapper>
        <div className="titleBarang">
        <h1>Barang</h1>
        <div className="buttons">
          <Button oke full
            onClick={() => {
              toggleModalRincian(true)
            }}
          >
            Tandai Sudah Terjual
          </Button>
        </div>
        </div>
        <div className="menu-dashboard">
          <div className="photo"/>
          <div className="item">
            <div className="item-menu">
            <p className="title">Nama</p>
            <p>:</p>
            <p className="item-content">Handphone Samsung</p>
            </div>
            <Menu title={"Kategori"} content={"Elektronik"}/>
            <Menu title={"Type"} content={"Handphone"}/>
            <Menu title={"Brand"} content={"Samsung"}/>
            <Menu title={"Model"} content={"A50s 128 GB"}/>
            <Menu title={"Kondisi"} content={"Bagus"}/>
            <Menu title={"Grade"} content={"A/95"}/>
            <Menu title={"Nilai Pinjaman"} content={"Rp. 2,000,000"}/>
            <Menu title={"Nilai Taksiran"} content={"Rp. 2,000,000"}/>
            <Menu title={"Jatuh Tempo"} content={"Rp. 12 Maret 2020"}/>
            </div>
        </div>
        <TabContent
          tabs={tabs}
          content={
            queryTab === 'kelengkapan' ?
            <div>
              kelengkapan
            </div> :
            queryTab === 'aksesoris' ?
            <div>
              aksesoris
            </div> :
            queryTab === 'kondisi' &&
            <div>
              kondisi
            </div>
          }
        />
        <div className="table-barang">
        <Table
          thead={
            <tr>
              <th>Nama Barang</th>
              <th>Cabang</th>
              <th>Kategori</th>
              <th>Lokasi Gudang</th>
              <th>Aksi</th>
            </tr>
          }
          tbody={
            <>
              <tr>
                <td>
                  <ul>
                    <li>Samsung A50s</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Cimone</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Elektronik</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Bandung</li>
                  </ul>
                </td>
                <td>
                  <div className="buttons">
                    <Button full outlineSecondary>
                      Details
                    </Button>
                  </div>
                </td>
              </tr>
              {/* <tr>
                <td colSpan={5}>Tidak ada permohonan</td>
              </tr> */}
            </>
          }
        />
        </div>
      </Wrapper>
    </Fragment>
  );
};

const ProfileMobile = styled.div`
  display: none;
  margin-top: 30px;
  width: 70%;
  background-color: white;
  border-top-right-radius: 100px;
  border-bottom-right-radius: 100px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.06);
  .wrapper {
    padding: 20px 125px 20px 15px;
    display: flex;
    position: relative;
    .user-info {
      h1 {
        font-size: 1.6rem;
        margin-bottom: 1.4rem;
        font-weight: 600;
        color: ${colors.primary};
      }
      h2 {
        font-size: 1rem;
        margin-bottom: 0.2rem;
        font-weight: 600;
      }
      p {
        font-size: 0.8rem;
        line-height: 1.2;
      }
    }
    .user-image {
      position: absolute;
      right: 13px;
      top: 50%;
      transform: translateY(-50%);
      background-color: lightblue;
      border-radius: 100px;
      height: 100px;
      width: 100px;
      overflow: hidden;
      img {
        width: 100%;
      }
    }
  }
  @media ${device.tablet} {
    display: block;
    width: 70%;
  }
  @media ${device.mobileL} {
    display: block;
    width: 95%;
  }
`;

const Wrapper = styled.div`
  @media ${device.tablet} {
    margin: 20px 0 ${sizes.sidebarMobile} 0;
    padding: 0 15px;
  }
  background-color: ${colors.white};
  margin: ${sizes.navbarHeight} 0 0 ${sizes.sidebarWidth};
  padding: 28px;
  .table-wrapper {
    @media ${device.tablet} {
      display: none;
    }
  }
  h1 {
    @media ${device.tablet} {
      display: none;
    }
    font-size: 2rem;
    font-weight: bold;
    color: ${colors.primary};
  }
  .menu-dashboard {
    @media ${device.tablet} {
      flex-direction: column;
    }
    padding: 8px 120px 12px 0px;
    margin-top: 3.6rem;
    display: flex;
    button {
      @media ${device.tablet} {
        margin: 0 0 1rem 0;
        padding: 24px 54px 48px 24px;
        border-radius: 8px;
      }
      margin-right: 1rem;
      text-align: left;
      img {
        @media ${device.tablet} {
          width: 60px;
          left: unset;
          right: 18px;
        }
      }
      span {
        @media ${device.tablet} {
          font-size: 1.2rem;
          font-weight: bold;
          opacity: 0.8;
        }
      }
    }
  }
  .titleBarang{
    display:flex;
    flex-direction:row;
    justify-content:space-between
  }
  .table-barang{
    padding:20px;
    border:1px solid black;
  }
  .photo{
    width:250px;
    height:170px;
    background-color:black;
    margin-right:20px
  }
  .item{
    display:flex;
    flex-direction:column;
  }
  .item-menu{
    display:flex;
    flex-direction:row;
    margin-top:5px
  }
  .item--content{
    min-width:150px
  }
  .title{
    min-width:150px
  }
  .info {
    @media ${device.tablet} {
      display: none;
    }
    padding-bottom: 0.6rem;
    margin-top: 2rem;
    display: flex;
    .current-transaction {
      font-size: 12px;
      font-weight: 500;
      background-color: rgba(99, 43, 141, 1);
      padding: 12px 28px;
      border-radius: 100px;
      color: white;
    }
  }
  .content-dashboard {
    background-color: white;
    flex-direction: column;
    display: flex;
    align-items: center;
    padding: 10px;
    border-style: solid;
    border-color: black;
    border-width: 0.5px;
    box-shadow: 10px 15px 10px rgba(241, 201, 236, 0.25);
    border-radius: 10px;
    min-width: 150px;
    margin: 10px;
  }
`;