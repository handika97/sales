import React, { Fragment, useState, useEffect } from "react";
import { useLocation, useHistory } from "react-router-dom";
import { connect } from "react-redux";

import styled from "styled-components";
import { device, colors } from "../Layout";
import Axios from "axios";

import Brand from "../../components/atoms/Brand";
import Button from "../../components/atoms/Button";

import eye from "../../assets/img/svg/eye.svg";
import eyeSlash from "../../assets/img/svg/eye-slash.svg";
import signBoard from "../../assets/img/jpg/sign-board.jpeg";

import endpoints from "../../endpoints";

import { setUserCs, setIsAuth } from "../../redux/actions";

// eslint-disable-next-line no-extend-native
Date.prototype.toIsoString = function () {
  var tzo = -this.getTimezoneOffset(),
    dif = tzo >= 0 ? "+" : "-",
    pad = function (num) {
      var norm = Math.floor(Math.abs(num));
      return (norm < 10 ? "0" : "") + norm;
    };
  return (
    this.getFullYear() +
    "-" +
    pad(this.getMonth() + 1) +
    "-" +
    pad(this.getDate()) +
    "T" +
    pad(this.getHours()) +
    ":" +
    pad(this.getMinutes()) +
    ":" +
    pad(this.getSeconds()) +
    dif +
    pad(tzo / 60) +
    ":" +
    pad(tzo % 60)
  );
};

const Login = ({ dispatch }) => {
  const [npp, setNpp] = useState("112244");
  const [password, setPass] = useState("123456");
  const [errCode, setErrCode] = useState(0);
  const [errMsg, setErrMsg] = useState("Oops, ada kesalahan. Mohon hubungi CS");
  const [isLoading, setIsLoading] = useState(false);
  const [passHidden, setPassHidden] = useState(true);

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    document.title = "Login - Raja Gadai";
    authenticate();
    console.log(process.env.REACT_APP_URL_API_HRIS);
  });

  const login = (e) => {
    e.preventDefault();
    setIsLoading(true);
    setErrCode(0);
    Axios.post(
      endpoints.login,
      {
        npp,
        password,
      },
      {
        headers: {
          Authorization: process.env.REACT_APP_API_AUTHORIZATION,
          "Device-ID": process.env.REACT_APP_API_DEVICE_ID,
          "Device-Type": "web",
        },
      }
    )
      .then((res) => {
        setIsLoading(false);
        dispatch(setUserCs(res.data));
        const data = res.data.data;

        localStorage.setItem(
          "token",
          JSON.stringify({
            access_token: data.access_token,
            at_exp: data.access_token_exp,
            refresh_token: data.refresh_token,
            rt_exp: data.refresh_token_exp,
          })
        );

        localStorage.setItem(
          "user",
          JSON.stringify({
            email: data.email,
            firstname: data.firstname,
            lastname: data.lastname,
            level: data.level,
            id: data.id,
            level_name: data.level_name,
            npp: data.npp,
            phone: data.phone,
            telegram: data.telegram,
            working_unit: data.working_unit,
            working_unit_name: data.working_unit_name,
          })
        );
        sessionStorage.setItem(
          "session",
          JSON.stringify({
            hash: res.data.data.session,
            expired: res.data.data.session_expired.split("Z")[0] + "+07:00",
            loginAt: new Date().toIsoString(),
          })
        );
      })
      .catch((err) => {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        sessionStorage.removeItem("session");
        loginErr(err.response);
        dispatch(setIsAuth(false));
      });
  };

  const authenticate = () => {
    if (sessionStorage.session && localStorage.token) {
      dispatch(setIsAuth(true));
      const { from } = location.state || { from: { pathname: "/" } };
      history.replace(from);
      return;
    }
    dispatch(setIsAuth(false));
  };

  const loginErr = (err) => {
    if (err.status) {
      if (err.status === 400) {
        setErrCode(1);
        setErrMsg(err.data.error_message);
        setIsLoading(false);
      }
    }
    setErrCode(1);
    setIsLoading(false);
    setErrMsg("Mohon maaf ada kesalahan, mohon hubungi CS");
  };

  const showPass = () => {
    setPassHidden(!passHidden);
  };
  const onFocus = (e) => {
    e.target.parentElement.classList.add("focus");
  };
  const offFocus = (e) => {
    e.target.parentElement.classList.remove("focus");
  };
  return (
    <Fragment>
      <Wrapper>
        {isLoading ? (
          <div className="loader">
            <span>mohon tunggu...</span>
          </div>
        ) : (
          ""
        )}
        <div className="login">
          <div className="hero">
            <h1>Selamat Datang di</h1>
            <Brand />
          </div>
          <div className="login-form">
            <h1>Masuk</h1>
            <span>Silakan otentikasi diri anda terlebih dahulu</span>
            <form onSubmit={login}>
              <div className="group-input">
                <div className="icon icon-npp">
                  <label htmlFor="npp"></label>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 109.4 109.42"
                  >
                    <g id="Layer_2" data-name="Layer 2">
                      <g id="Layer_1-2" data-name="Layer 1">
                        <g id="people">
                          <path
                            className="cls-1"
                            d="M54.7,55.76A27.88,27.88,0,1,0,26.82,27.88,27.92,27.92,0,0,0,54.7,55.76Zm0-43.13A15.25,15.25,0,1,1,39.45,27.88,15.26,15.26,0,0,1,54.7,12.63Z"
                          />
                          <path
                            className="cls-1"
                            d="M105.69,77.25q-3.54-2.22-7.27-4.12a96.35,96.35,0,0,0-87.44,0Q7.25,75,3.71,77.25A8,8,0,0,0,0,84v25.38a0,0,0,0,0,0,0H109.37a0,0,0,0,0,0,0V84A8,8,0,0,0,105.69,77.25ZM96.77,96.76a0,0,0,0,1,0,0H12.66a0,0,0,0,1,0,0V88.3a2.87,2.87,0,0,1,1.51-2.53,83.59,83.59,0,0,1,81.12,0,2.87,2.87,0,0,1,1.51,2.53Z"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </div>
                <input
                  onFocus={(e) => onFocus(e)}
                  onBlur={(e) => offFocus(e)}
                  value={npp}
                  onChange={(e) => setNpp(e.target.value)}
                  type="text"
                  id="npp"
                  className="npp"
                  placeholder="NPP"
                />
              </div>
              <div className="group-input">
                <div className="icon icon-npp">
                  <label htmlFor="password"></label>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 96.02 117.97"
                  >
                    <g id="Layer_2" data-name="Layer 2">
                      <g id="Layer_1-2" data-name="Layer 1">
                        <circle
                          className="cls-1"
                          cx="48.01"
                          cy="78.89"
                          r="10.96"
                        />
                        <path
                          className="cls-1"
                          d="M86,39.82H76.62V28.61c0,.25,0,.5,0,.74V27.72a28.6,28.6,0,0,0-57.17,0c0,.12,0,.24,0,.36V29c0-.12,0-.25,0-.37V39.82H10.08A10.08,10.08,0,0,0,0,49.9v58A10.07,10.07,0,0,0,10.08,118H86A10.07,10.07,0,0,0,96,107.9v-58A10.07,10.07,0,0,0,86,39.82ZM30.41,27.72h0a17.61,17.61,0,0,1,35.17,0h0v12.1H30.41ZM85,107H11V50.82H85Z"
                        />
                      </g>
                    </g>
                  </svg>
                </div>
                <div onClick={showPass} className="eye">
                  <img src={passHidden ? eye : eyeSlash} alt="" />
                </div>
                <input
                  onFocus={onFocus}
                  onBlur={offFocus}
                  value={password}
                  onChange={(e) => setPass(e.target.value)}
                  type={passHidden ? "password" : "text"}
                  id="password"
                  className="password"
                  placeholder="Password"
                />
              </div>
              {errCode === 1 ? (
                <div className="error">
                  <span>{errMsg}</span>
                </div>
              ) : (
                ""
              )}
              <Button secondary full>
                masuk
              </Button>
            </form>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

const Wrapper = styled.div`
  .loader {
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 1;
    background-color: rgba(0, 0, 0, 0.7);
    span {
      @media ${device.tablet} {
        font-size: 1rem;
      }
      color: white;
      font-size: 2rem;
    }
  }
  @media ${device.tablet} {
    padding: 0 18px;
  }
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background-color: #e9ecef;
  .login {
    @media ${device.tablet} {
      height: auto;
    }
    width: 1000px;
    height: 500px;
    display: flex;
    border-radius: 14px;
    overflow: hidden;
    box-shadow: 0 7px 8px rgba(0, 0, 0, 0.1);
    @media ${device.tablet} {
      flex-direction: column;
    }
  }
  .hero,
  .login-form {
    @media ${device.tablet} {
      height: auto;
    }
    padding: 70px;
    height: 100%;
    h1 {
      font-size: 28px;
      font-weight: bold;
      color: ${colors.primary};
      margin-bottom: 5rem;
    }
  }
  .hero {
    @media ${device.tablet} {
      width: 100%;
      padding-bottom: 0;
      h1 {
        display: none;
      }
    }
    width: 50%;
    background-color: ${colors.white};
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    .brand {
      width: 300px;
      @media ${device.tablet} {
        width: 200px;
      }
    }
  }
  .login-form {
    @media ${device.tablet} {
      width: 100%;
      background: ${colors.white};
      padding: 50px 28px;
      color: ${colors.black};
    }
    width: 50%;
    min-height: 100px;
    background: linear-gradient(
        0deg,
        rgba(122, 65, 163, 0.9),
        rgba(122, 65, 163, 0.9)
      ),
      url(${signBoard}) right;
    background-size: cover;
    color: white;
    transition: box-shadow 0.1s;
    h1 {
      @media ${device.tablet} {
        color: ${colors.black};
        margin-bottom: 2rem;
      }
      color: white;
      margin-bottom: 3.7rem;
    }
    span {
      font-size: 14px;
    }
    form {
      margin-top: 1rem;
      .group-input {
        position: relative;
        margin-top: 1rem;
        background-color: white;
        width: 100%;
        border-radius: 4px;
        overflow: hidden;
        box-shadow: 0 6px 8px rgba(0, 0, 0, 0.07);
        &.focus {
          outline: none;
          border: none;
          box-shadow: 0 0 2px 3px rgba(99, 43, 141, 0.5);
        }
        .eye {
          cursor: pointer;
          position: absolute;
          right: 12px;
          top: 50%;
          transform: translateY(-50%);
          width: 20px;
          img {
            width: 100%;
          }
        }
        svg {
          width: 20px;
          fill: #632d8b;
          @media ${device.tablet} {
            fill: rgba(0, 0, 0, 0.4);
          }
        }
        label {
          position: absolute;
          left: 0;
          height: 100%;
          width: 100%;
        }
        .icon {
          position: absolute;
          left: 0;
          width: 46px;
          height: 100%;
          padding: 0 12px;
          border-right: 1.3px solid rgba(99, 43, 141, 0.2);
          display: flex;
          align-items: center;
          img {
            width: 100%;
          }
        }
        input {
          width: 100%;
          height: 100%;
          padding: 14px 14px 14px 64px;
          outline: none;
          border: none;
          font-size: 14px;
          color: ${colors.primary};
          font-weight: 600;
          @media ${device.tablet} {
            color: rgba(0, 0, 0, 0.7);
          }
          &#password {
            padding-right: 50px;
          }
          &::placeholder {
            color: rgba(99, 43, 141, 0.5);
            font-weight: normal;
            @media ${device.tablet} {
              color: rgba(0, 0, 0, 0.4);
            }
          }
        }
      }
      .error {
        margin-top: 1rem;
        border: 2px solid rgba(240, 83, 72);
        background-color: rgba(235, 64, 52, 0.07);
        padding: 12px 16px;
        border-radius: 4px;
        span {
          @media ${device.tablet} {
            color: rgba(240, 83, 72);
          }
          color: rgba(255, 234, 232);
        }
      }
      button {
        margin-top: 1rem;
      }
    }
  }
`;

export default connect()(Login);
