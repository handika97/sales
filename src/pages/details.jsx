import React, { Fragment, useEffect } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

import { colors, sizes, device } from "./Layout";
import Button from "../components/atoms/Button";
import Table from "../components/cells/Table";
import Navbar from "../components/cells/Navbar";
import Sidebar from "../components/cells/Sidebar";
import gadaiBaru from "../assets/img/png/gadaiBaru.png";
import gadaiPerpanjang from "../assets/img/png/gadaiPerpanjang.png";
import gadaiTebus from "../assets/img/png/gadaiTebus.png";
import defaultProfile from "../assets/img/jpg/default_profile.jpg";
import Search from "../components/atoms/Search";
import Select from "../components/atoms/Picker";

const Dashboard = () => {
  useEffect(() => {
    document.title = "Dashboard - Raja Gadai";
  });
  const history = useHistory();
  return (
    <Fragment>
      <Navbar />
      <Sidebar />
      <ProfileMobile>
        <div className="wrapper">
          <div className="user-info">
            <h1>Welcome</h1>
            <h2>Nurhidayah</h2>
            <p>Customer Service - Cabang CIMONE</p>
          </div>
          <div className="user-image">
            <img src={defaultProfile} alt="default" />
          </div>
        </div>
      </ProfileMobile>
      <Wrapper>
        <h1>Barang</h1>
        <div className="menu-dashboard">
          <Select />
          <Search placeholder="Cari" />
        </div>

        <Table
          thead={
            <tr>
              <th>Nama Barang</th>
              <th>Cabang</th>
              <th>Kategori</th>
              <th>Lokasi Gudang</th>
              <th>Aksi</th>
            </tr>
          }
          tbody={
            <>
              <tr>
                <td>
                  <ul>
                    <li>Samsung A50s</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Cimone</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Elektronik</li>
                  </ul>
                </td>
                <td>
                  <ul>
                    <li>Bandung</li>
                  </ul>
                </td>
                <td>
                  <div className="buttons">
                    <Button full outlineSecondary>
                      Details
                    </Button>
                  </div>
                </td>
              </tr>
              {/* <tr>
                <td colSpan={5}>Tidak ada permohonan</td>
              </tr> */}
            </>
          }
        />
      </Wrapper>
    </Fragment>
  );
};

const ProfileMobile = styled.div`
  display: none;
  margin-top: 30px;
  width: 70%;
  background-color: white;
  border-top-right-radius: 100px;
  border-bottom-right-radius: 100px;
  box-shadow: 0 4px 12px rgba(0, 0, 0, 0.06);
  .wrapper {
    padding: 20px 125px 20px 15px;
    display: flex;
    position: relative;
    .user-info {
      h1 {
        font-size: 1.6rem;
        margin-bottom: 1.4rem;
        font-weight: 600;
        color: ${colors.primary};
      }
      h2 {
        font-size: 1rem;
        margin-bottom: 0.2rem;
        font-weight: 600;
      }
      p {
        font-size: 0.8rem;
        line-height: 1.2;
      }
    }
    .user-image {
      position: absolute;
      right: 13px;
      top: 50%;
      transform: translateY(-50%);
      background-color: lightblue;
      border-radius: 100px;
      height: 100px;
      width: 100px;
      overflow: hidden;
      img {
        width: 100%;
      }
    }
  }
  @media ${device.tablet} {
    display: block;
    width: 70%;
  }
  @media ${device.mobileL} {
    display: block;
    width: 95%;
  }
`;

const Wrapper = styled.div`
  @media ${device.tablet} {
    margin: 20px 0 ${sizes.sidebarMobile} 0;
    padding: 0 15px;
  }
  background-color: ${colors.white};
  margin: ${sizes.navbarHeight} 0 0 ${sizes.sidebarWidth};
  padding: 28px;
  .table-wrapper {
    @media ${device.tablet} {
      display: none;
    }
  }
  h1 {
    @media ${device.tablet} {
      display: none;
    }
    font-size: 2rem;
    font-weight: bold;
    color: ${colors.primary};
  }
  .menu-dashboard {
    @media ${device.tablet} {
      flex-direction: column;
    }
    padding: 8px 120px 12px 0px;
    margin-top: 3.6rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    button {
      @media ${device.tablet} {
        margin: 0 0 1rem 0;
        padding: 24px 54px 48px 24px;
        border-radius: 8px;
      }
      margin-right: 1rem;
      text-align: left;
      img {
        @media ${device.tablet} {
          width: 60px;
          left: unset;
          right: 18px;
        }
      }
      span {
        @media ${device.tablet} {
          font-size: 1.2rem;
          font-weight: bold;
          opacity: 0.8;
        }
      }
    }
  }
  .info {
    @media ${device.tablet} {
      display: none;
    }
    padding-bottom: 0.6rem;
    margin-top: 2rem;
    display: flex;
    .current-transaction {
      font-size: 12px;
      font-weight: 500;
      background-color: rgba(99, 43, 141, 1);
      padding: 12px 28px;
      border-radius: 100px;
      color: white;
    }
  }
  .content-dashboard {
    background-color: white;
    flex-direction: column;
    display: flex;
    align-items: center;
    padding: 10px;
    border-style: solid;
    border-color: black;
    border-width: 0.5px;
    box-shadow: 10px 15px 10px rgba(241, 201, 236, 0.25);
    border-radius: 10px;
    min-width: 150px;
    margin: 10px;
  }
`;

export default Dashboard;
