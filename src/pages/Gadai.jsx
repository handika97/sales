import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import styled from 'styled-components';
import { device, colors, sizes } from './Layout';

import Table from '../components/cells/Table';
import Button from '../components/atoms/Button';
import Search from '../components/atoms/Search';
// import Modal from '../components/cells/Modal';

import gadaiBaru from '../assets/img/png/gadaiBaru.png';
import gadaiPerpanjang from '../assets/img/png/gadaiPerpanjang.png';
import gadaiTebus from '../assets/img/png/gadaiTebus.png';

import { retrieveReqLoan } from '../redux/actions';
import endPoints from '../endpoints';
import Axios from 'axios';

const Gadai = ({ dispatch, itemsLoan, isLoading, totalPages, requestedLoan, totalItems }) => {
  const [ filterTable, setFilterTable ] = useState(1);
  const [ currentPage, setCurrentPage ] = useState(1);
  const [ search, setSearch ] = useState('');

  useEffect(() => {
    document.title = 'Gadai - Raja Gadai';
  }, []);
  
  useEffect(() => {
    if (filterTable === 1) {
      Axios.post(`${endPoints.retrieveRequestedLoan}?perm=read&limit=4&page=${currentPage}&search=${search}&sort_field&sort_type`, {}, {
        headers: {
          'Authorization': process.env.REACT_APP_API_AUTHORIZATION,
          'Device-ID': process.env.REACT_APP_API_DEVICE_ID,
          'Device-Type': 'web',
          'Device-Session': JSON.parse(sessionStorage.session).hash,
          'Staff-Token': JSON.parse(localStorage.token).access_token     
        }
      }).then(res => {
        dispatch(retrieveReqLoan(res.data));
        console.log(res.data.data)
      }).catch(err => {
        console.log(err.response)
      })
    }
  }, [currentPage, dispatch, filterTable, search]);

  const history = useHistory();

  return (
    <>
      <Wrapper>
        <h1>Data Transaksi Gadai</h1>
        <div className="menu-dashboard">
          <Button onClick={() => history.replace('/gadai/new')} withImage><img src={gadaiBaru} alt=""/><span>gadai baru</span></Button>
          <Button withImage><img src={gadaiTebus} alt=""/><span>tebus gadai</span></Button>
          <Button withImage><img src={gadaiPerpanjang} alt=""/><span>perpanjang gadai</span></Button>
          <Button secondary>Referensi Harga Taksiran</Button>
        </div>
        <div className="table-top">
          <Search value={search} placeholder="Cari" onChange={(e) => setSearch(e.target.value)} />
          <div className="info">
            <div
              onClick={() => setFilterTable(1)}
              className={
                filterTable === 1 ?
                  'table-filter current' :
                  'table-filter'
              }
            >
              <span>Permohonan</span>
            </div>
            <div
              onClick={() => setFilterTable(2)}
              className={
                filterTable === 2 ?
                  'table-filter current' :
                  'table-filter'
              }
            >
              <span>Gadai Berjalan</span>
            </div>
            <div
              onClick={() => setFilterTable(3)}
              className={
                filterTable === 3 ?
                  'table-filter current' :
                  'table-filter'
              }
            >
              <span>Jatuh Tempo</span>
            </div>
          </div>
        </div>

        <Table
          thead={(
            <tr>
              <th width="60">No</th>
              <th>Transaksi</th>
              <th>Barang</th>
              <th>Nasabah</th>
              <th>Status</th>
              <th width="170">Aksi</th>
            </tr>
          )}
          tbody={(
            <>
              {
                isLoading ?
                  (
                    <tr>
                      <td colSpan={6}>
                        Sedang memuat data..
                      </td>
                    </tr>  
                  ) : (
                    itemsLoan.length !== 0 ?
                      itemsLoan.map((item, i) => (
                        <tr key={item.id}>
                          <td>{(i + 1) + Math.ceil(totalItems / totalPages) * (currentPage - 1)}</td>
                          <td>
                            <ul>
                              <li>
                                FPT : { !item.fpt_code ? 'N/A' : item.fpt_code }
                              </li>
                              <li>
                                SBG : { !item.sbg_code ? 'N/A' : item.sbg_code }
                              </li>
                              <li>
                                Taksiran : N/A
                              </li>
                              <li>
                                Pinjaman : N/A
                              </li>
                            </ul>
                          </td>
                          <td>
                            <ul>
                              <li>
                                QR Code : { !item.qr_code ? 'N/A' : item.qr_code }
                              </li>
                              <li>
                                { !item.type_name ? '-' : item.type_name } : Grade B
                              </li>
                              <li>
                                { !item.brand_name ? '-' : item.brand_name } { !item.model_name ? '-' : item.model_name }
                              </li>
                              <li>&nbsp;</li>
                            </ul>
                          </td>
                          <td>
                            <ul>
                              <li>No. CIF : { !item.customer_cif ? 'N/A' : item.customer_cif }</li>
                              <li>{ !item.customer_name ? '-' : item.customer_name }</li>
                              <li>{ !item.customer_phone ? '-' : item.customer_phone }</li>
                              <li>Cabang Cimone</li>
                            </ul>
                          </td>
                          <td>
                            <span className="status">{ !item.status_trx ? '-' : item.status_trx }</span>
                          </td>
                          <td>
                            <div className="buttons">
                              <Button full outlineSecondary>transaksi detail</Button>
                              <Button full outlineSecondary>barang detail</Button>
                            </div>
                          </td>
                        </tr>
                      )) : (
                        <tr>
                          <td colSpan={6}>
                            Data tidak ditemukan
                          </td>
                        </tr>  
                      )
                  )
              }
            </>
          )}
        />
        <div className="wrap-pagination">
          <div className="pagination">
            <button
              className={
                currentPage <= 1 ?
                  'disabled' :
                  ''
              }
              onClick={() => {
                if (currentPage <= 1) return;
                setCurrentPage(parseInt(currentPage) - 1)
              }}
            >
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.82 132.37">
                <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M70.88,121a6.64,6.64,0,1,1-9.39,9.39L1.94,70.88a6.65,6.65,0,0,1,0-9.39L61.49,1.94a6.64,6.64,0,1,1,9.39,9.39L16,66.19Z"/></g></g>
              </svg>
            </button>
            <div>
              <input type="number" value={currentPage}
                onBlur={(e) => {
                  if (Number(e.target.value) <= 0) {
                    setCurrentPage(1);
                    return
                  }
                }}
                onChange={(e) => {
                  if (Number(e.target.value) < 0) {
                    setTimeout(() => {
                      setCurrentPage(1);
                    }, 1000)
                  }
                  if (Number(e.target.value) > Number(totalPages)) {
                    setCurrentPage(totalPages);
                    return;
                  }
                  setCurrentPage(e.target.value);
                }}  
              />
              <span> dari <strong>{ totalPages }</strong></span>
            </div>
            <button
              className={
                currentPage >= totalPages ?
                  'disabled' :
                  ''
              }
              onClick={() => {
                if (currentPage === totalPages) return;
                setCurrentPage(parseInt(currentPage) + 1);
              }}
            >
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 72.82 132.37">
                <g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path d="M70.88,121a6.64,6.64,0,1,1-9.39,9.39L1.94,70.88a6.65,6.65,0,0,1,0-9.39L61.49,1.94a6.64,6.64,0,1,1,9.39,9.39L16,66.19Z"/></g></g>
              </svg>
            </button>
          </div>
        </div>
      </Wrapper>
      {/* <Modal /> */}
    </>
  );
}

const mapStateToProps = state => ({
  requestedLoan: state.Loan.requested,
  isLoading: state.Loan.isLoading,
  itemsLoan: state.Loan.requested.items,
  totalPages: state.Loan.requested.total_pages,
  totalItems: state.Loan.requested.total_items
})

export default connect(mapStateToProps)(Gadai);

const Wrapper = styled.div`
  @media ${device.tablet} {
    margin: 0;
    padding: 20px 15px ${sizes.sidebarMobile};
  }
  background-color: ${colors.white};
  margin: ${sizes.navbarHeight} 0 0 ${sizes.sidebarWidth};
  padding: 28px;
  .table-wrapper {
    /* margin-top: .7rem; */
    @media ${device.tablet} {
      margin-top: .3rem;
    }
  }
  h1 {
    @media ${device.tablet} {
      font-size: 22px;
    }
    font-size: 2rem;
    font-weight: bold;
    color: ${colors.primary};
  }
  .menu-dashboard {
    @media ${device.tablet} {
      display: flex;
      flex-wrap: wrap;
      margin-top: 2rem;
    }
    padding: 8px 0 12px;
    margin-top: 3.6rem;
    display: flex;
    button {
      @media ${device.tablet} {
        &:not(:last-child) {
          min-width: unset;
          text-align: left;
          padding: 9px 9px 9px 40px;
          margin: 0 5px 5px 0;
          span {
            font-size: 9px;
          }
          img {
            left: 8px;
          }
        }
      }
      &:not(:first-child) {
        @media ${device.tablet} {
          margin-left: 0;
        }
        margin-left: 1rem;
      }
      &:last-child {
        @media ${device.mobileL} {
          margin: .8rem 0 0 0 !important;
        }
        @media ${device.tablet} {
          margin-left: auto;
          font-size: 12px;
          padding: 9px 12px;
        }
        margin-left: auto;
      }
    }
  }
  .table-top {
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: ${colors.white};
    position: sticky;
    top: ${sizes.navbarHeight};
    padding-bottom: .6rem;
    @media ${device.tablet} {
      top: 0;
    }
    @media ${device.mobileL} {
      top: 0px;
      padding-top: 1.6rem;
      flex-direction: column;
      align-items: flex-start;
      justify-content: center;
    }
    padding-top: 2rem;
  }
  .search {
    order: 2;
    @media ${device.mobileL} {
      order: 1;
      input {
        padding: 4px 30px 4px 6px;
      }
      button {
        padding: 5px;
        width: 30px;
        svg {
          height: 100%;
        }
      }
    }
  }
  .info {
    @media ${device.mobileL} {
      margin-top: .6rem;
      order: 2;
    }
    display: flex;
    order: 1;
    .table-filter {
      @media ${device.tablet} {
        padding: 9px 10px;
      }
      &:not(:first-child) {
        @media ${device.tablet} {
          margin-left: .3rem;
        }
        margin-left: .7rem;
      }
      font-size: 12px;
      font-weight: 500;
      background-color: rgba(0, 0, 0, .1);
      padding: 12px 28px;
      border-radius: 100px;
      color: rgba(0, 0, 0, .5);
      cursor: pointer;
      &.current {
        background-color: rgba(99, 43, 141, 1);
        color: white;
      }
    }
  }
  .wrap-pagination {
    @media ${device.tablet} {
      justify-content: center;
    }
    display: flex;
    justify-content: flex-end;
    margin: .7rem 0 2rem;
  }
  .pagination {
    border-radius: 100px;
    background-color: white;
    box-shadow: 0 2px 8px rgba(0, 0, 0, .06);
    display: inline-flex;
    align-items: center;
    padding: 4px 6px;
    div {
      margin: 0 .4rem;
      span {
        font-size: 12px;
        strong {
          font-size: 14px;
          font-weight: bold;
          margin: 0 .3rem 0 .5rem; 
        }
      }
    }
    input {
      font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
      font-size: 14px;
      font-weight: bold;
      width: 30px;
      height: 100%;
      border: none;
      background-color: transparent;
      outline: none;
      text-align: center;
      &::-webkit-outer-spin-button,
      &::-webkit-inner-spin-button,
      &[type=number] {
        -webkit-appearance: none;
        margin: 0;
        -moz-appearance: textfield;
      }
    }
    button {
      cursor: pointer;
      border: none;
      background-color: transparent;
      border-radius: 100px;
      height: 36px;
      width: 36px;
      padding: 8px;
      outline: none;
      &.disabled {
        cursor: context-menu;
        &:hover {
          svg {
            fill: rgba(0, 0, 0, .2);
          }
        }
      }
      svg {
        height: 100%;
        fill: rgba(0, 0, 0, .2);
        transition: .1s;
      }
      &:hover {
        svg {
          fill: rgba(0, 0, 0, .7);
        }
      }
      &:last-child {
        svg {
          transform: rotate(180deg);
        }
      }
    }
  }
`;
