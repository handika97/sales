// import styled from 'styled-components';

export const colors = {
  primary: '#632B8D',
  secondary: '#A9CE37',
  black: '#424242',
  white: '#F7F6F6'
};

export const sizes = {
  navbarHeight: '80px',
  navbarMobile: '120px',
  sidebarWidth: '120px',
  sidebarMobile: '80px'
}

const size = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px'
};

export const device = {
  mobileS: `(max-width: ${size.mobileS})`,
  mobileM: `(max-width: ${size.mobileM})`,
  mobileL: `(max-width: ${size.mobileL})`,
  tablet: `(max-width: ${size.tablet})`,
  laptop: `(max-width: ${size.laptop})`,
  laptopL: `(max-width: ${size.laptopL})`,
  desktop: `(max-width: ${size.desktop})`,
  desktopL: `(max-width: ${size.desktop})`
};
