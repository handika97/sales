export default {
  login: `${process.env.REACT_APP_URL_API_HRIS}/login`,
  logout: `${process.env.REACT_APP_URL_API_HRIS}/logout`,
  refreshToken: `${process.env.REACT_APP_URL_API_HRIS}/refresh_token`,
  retrieveRequestedLoan: `${process.env.REACT_APP_URL_API_LOAN}/loan/request/cs/branch`
}