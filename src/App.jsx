import React, { Fragment, useEffect, useState } from "react";
import store from "./redux/store";
import { Switch, Route, Redirect } from "react-router-dom";
import endPoints from "./endpoints";
import { connect } from "react-redux";

import Login from "./pages/auth/Login";
// import Dashboard from "./pages/Dashboard";
import Gadai from "./pages/Gadai";
import GadaiBaru from "./pages/GadaiBaru";
import Dashboard from "./pages/Dashboard";
import Barang from "./pages/Barang";
import Navbar from "./components/cells/Navbar";
import Sidebar from "./components/cells/Sidebar";
import Axios from "axios";

// eslint-disable-next-line no-extend-native
Date.prototype.toIsoString = function () {
  var tzo = -this.getTimezoneOffset(),
    dif = tzo >= 0 ? "+" : "-",
    pad = function (num) {
      var norm = Math.floor(Math.abs(num));
      return (norm < 10 ? "0" : "") + norm;
    };
  return (
    this.getFullYear() +
    "-" +
    pad(this.getMonth() + 1) +
    "-" +
    pad(this.getDate()) +
    "T" +
    pad(this.getHours()) +
    ":" +
    pad(this.getMinutes()) +
    ":" +
    pad(this.getSeconds()) +
    dif +
    pad(tzo / 60) +
    ":" +
    pad(tzo % 60)
  );
};

const App = ({ dispatch }) => {
  // const [sessionExp, setSessionExp] = useState();
  useEffect(() => {
    if (localStorage.token) {
      const atExp = JSON.parse(localStorage.token).at_exp;
      const parseAtExp = new Date(atExp.split("Z")[0]);
      const currentTime = new Date();
      const timeDiff = parseAtExp - currentTime;
      // console.log('Refresh token dalam ' + Math.floor((timeDiff - 2 * 60000) / 60000) + ' menit');

      setInterval(() => {
        refreshToken();
      }, timeDiff - 2 * 60000);
    }
    if (sessionStorage.session) {
      const currentTime = Date.parse(new Date().toIsoString());
      const sessionExp = Date.parse(
        JSON.parse(sessionStorage.session).expired.split("Z")[0]
      );
      const selisih = sessionExp - currentTime;
      // const day = 1000 * 60 * 60 * 24;
      // console.log(currentTime);
      // console.log(sessionExp);
      // console.log((sessionExp - currentTime) / day);
      // console.log(selisih)
      setInterval(() => {
        logout();
        alert("Session berakhir, silakan login kembali");
      }, selisih);
    }
  });
  const logout = () => {
    Axios.post(
      endPoints.logout,
      {},
      {
        headers: {
          Authorization: process.env.REACT_APP_API_AUTHORIZATION,
          "Device-ID": process.env.REACT_APP_API_DEVICE_ID,
          "Device-Type": "web",
          "Staff-Token": JSON.parse(localStorage.token).access_token,
          "Device-Session": JSON.parse(sessionStorage.session).hash,
        },
      }
    )
      .then(() => {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        sessionStorage.removeItem("session");
        window.location.reload(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };
  const refreshToken = () => {
    Axios.post(
      endPoints.refreshToken,
      {},
      {
        headers: {
          Authorization: process.env.REACT_APP_API_AUTHORIZATION,
          "Device-ID": process.env.REACT_APP_API_DEVICE_ID,
          "Device-Type": "web",
          "Staff-Token": JSON.parse(localStorage.token).refresh_token,
          "Device-Session": JSON.parse(sessionStorage.session).hash,
        },
      }
    )
      .then((res) => {
        const data = res.data.data;
        localStorage.setItem(
          "token",
          JSON.stringify({
            access_token: data.access_token,
            at_exp: data.access_token_exp,
            refresh_token: data.refresh_token,
            rt_exp: data.refresh_token_exp,
          })
        );
        // alert("Token berhasil direfresh");
      })
      .catch((err) => {
        // alert("Token gagal direfresh");
        console.log(err.response);
      });
  };
  return (
    <div className="App">
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>
        <PrivateRoute>
          <DefaultRoute />
        </PrivateRoute>
      </Switch>
    </div>
  );
};

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        store.getState().CSLogin.isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

const DefaultRoute = () => (
  <Fragment>
    <Navbar />
    <Sidebar />
    <Route exact path="/">
      <Dashboard />
    </Route>
    <Route exact path="/dashboard">
      <Gadai />
    </Route>
    <Route path="/barang">
      <Barang />
    </Route>
  </Fragment>
);

export default connect()(App);
