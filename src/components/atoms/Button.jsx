import React from 'react';
import styled, { css } from 'styled-components';

import { colors } from '../../pages/Layout';

const Button = (props) => {
  return (
    <Styles
      onClick={props.onClick}
      primary={props.primary}
      secondary={props.secondary}
      full={props.full}
      outlineSecondary={props.outlineSecondary}
      withImage={props.withImage}
      oke={props.oke}
      cancel={props.cancel}
      style={props.style}
    >
      { props.children }
    </Styles>
  )
}

const Styles = styled.button`
  padding: 14px 14px;
  min-width: 140px;
  font-size: 14px;
  font-weight: bold;
  text-transform: capitalize;
  border: none;
  background-color: ${colors.primary};
  color: white;
  border-radius: 4px;
  cursor: pointer;
  box-shadow: 0 6px 8px rgba(0, 0, 0, .1);
  transition: box-shadow .1s;
  &:focus {
    outline: none;
    border: none;
    box-shadow: 0 0 2px 3px rgba(99, 43, 141, .2);
  }
  ${props => props.primary && css`
    background-color: ${colors.primary};
    color: white;
  `};
  ${props => props.secondary && css`
    background-color: ${colors.secondary};
    color: white;
  `};
  ${props => props.outlineSecondary && css`
    border: 1px solid ${colors.secondary};
    background-color: transparent;
    color: ${colors.secondary};
    box-shadow: none;
    padding: 6px 12px;
    font-weight: normal;
    min-width: 80px;
    &:focus {
      outline: none;
      border: 1px solid ${colors.secondary};
      box-shadow: 0 0 2px 3px rgba(168, 204, 55, .3);
    }
  `};
  ${props => props.oke && css`
    border: 1px solid #4c7a34;
    background-color: #4c7a34;
    color: white;
    box-shadow: none;
    padding: 6px 12px;
    font-weight: normal;
    min-width: 80px;
    &:focus {
      outline: none;
      border: 1px solid #4c7a34;
      box-shadow: 0 0 2px 3px rgba(168, 204, 55, .3);
    }
  `};
  ${props => props.cancel && css`
    border: 1px solid #bababa;
    background-color: #bababa;
    color: white;
    box-shadow: none;
    padding: 6px 12px;
    font-weight: normal;
    min-width: 80px;
    &:focus {
      outline: none;
      border: 1px solid #bababa;
      box-shadow: 0 0 2px 3px rgba(186,186,186, .3);
    }
  `};
  ${props => props.full && css`
    width: 100%;
  `};
  ${props => props.withImage && css`
    padding: 14px 18px 14px 54px;
    position: relative;
    border: none;
    background-color: white;
    box-shadow: 0 4px 0 rgba(0, 0, 0, .1);
    border-radius: 4px;
    cursor: pointer;
    transition: .07s;
    &:active {
      transform: translateY(4px);
      box-shadow: 0 0px 0 rgba(0, 0, 0, .1);
    }
    &:focus {
      outline: none;
    }
    img {
      position: absolute;
      width: 30px;
      left: 12px;
      top: 50%;
      transform: translateY(-50%);
    }
    span {
      text-transform: capitalize;
      font-size: 14px;
      color: ${colors.primary};
    }
  `};
`;

export default Button;
