import React from 'react';
import styled from 'styled-components';

import logo from '../../assets/img/png/logo.png';
import logoBorder from '../../assets/img/png/logo-border.png';


const Brand = ({ bordered }) => {
  return (
    <Styles className="brand">
      { bordered ? (<img src={ logoBorder } alt="Raja Gadai Logo Border"/>) : (<img src={ logo } alt="Raja Gadai Logo"/>)}
    </Styles>
  );
}

const Styles = styled.div`
  width: 200px;
  img {
    width: 100%;
  }
`;


export default Brand;
