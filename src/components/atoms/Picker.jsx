import React from "react";

import styled from "styled-components";

export default ({ value, onChange, placeholder = "" }) => {
  return (
    <Styles className="search">
      <form onSubmit={(e) => e.preventDefault()}>
        <select name="Kategori" id="Kategori">
          <option value="Elektronik">Elektronik</option>
          <option value="Non Elektronik">Non Elektronik</option>
          <option value="Kendaraan">Kendaraan</option>
        </select>
        {/* <button>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 156.9 156.9">
            <g id="Layer_2" data-name="Layer 2">
              <g id="Layer_1-2" data-name="Layer 1">
                <path d="M126.14,63.07a63.07,63.07,0,1,0-63.07,63.07A63.14,63.14,0,0,0,126.14,63.07ZM63.07,113.49A50.43,50.43,0,1,1,113.5,63.07,50.42,50.42,0,0,1,63.07,113.49Z" />
                <path d="M156.19,146.91l-39.64-39.65a4.06,4.06,0,0,0-5.75,0l-3.53,3.53a4.06,4.06,0,0,0,0,5.75l39.64,39.65a2.44,2.44,0,0,0,3.43,0l5.85-5.86A2.41,2.41,0,0,0,156.19,146.91Z" />
              </g>
            </g>
          </svg>
        </button> */}
      </form>
    </Styles>
  );
};

const Styles = styled.div`
  form {
    width: 200px;
    position: relative;
    select {
      width: 90%;
      padding: 7px 38px 7px 10px;
      border-radius: 4px;
      border: 1.4px solid rgba(0, 0, 0, 0.2);
      background-color: transparent;
      outline: none;
      transition: 0.1s;
      font-size: 12px;
      &:focus {
        box-shadow: 0 0 2px 2px rgba(99, 43, 141, 0.3);
      }
    }
    button {
      height: 100%;
      width: 20%;
      position: absolute;
      right: 0;
      border-radius: 0px 4px 4px 0px;
      cursor: pointer;
      background-color: #632b8d;
      border: none;
      outline: none;
      padding: 0.47rem;
      svg {
        fill: rgba(0, 0, 0, 0.4);
        height: 100%;
      }
    }
  }
`;
