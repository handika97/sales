import React from 'react';
import styled from "styled-components";
import { useHistory, useRouteMatch, useLocation } from 'react-router-dom';

export default ({content = '', tabs = []}) => {
    const history = useHistory();
    const {path} = useRouteMatch();
    return <>
        <Wrapper>
            <Tabs>
                {tabs.map(tab => {
                    return <div
                        onClick={() => {
                            history.push(`${path}?tab=${tab}`)
                        }}
                    >
                        {tab}
                    </div>
                })}
            </Tabs>
            {/* <Content>
                {content}
            </Content> */}
        </Wrapper>
    </>
}

const Wrapper = styled.div`
    width: 100%;
    /* min-height: 300px; */
`;
const Tabs = styled.div`
    min-height: 40px;
    display: flex;
    div {
        min-width: 80px;
        border: 1px solid black;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 4px 1.5rem;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
        text-transform: capitalize;
        cursor: pointer;
    }
`;
const Content = styled.div`

`;
