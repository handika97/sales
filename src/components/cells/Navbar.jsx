import React, { Fragment } from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import Brand from '../atoms/Brand';
import { colors, device, sizes } from '../../pages/Layout';
import defaultProfile from '../../assets/img/jpg/default_profile.jpg';

const Navbar = () => {
  const fullname = localStorage.user ?
  JSON.parse(localStorage.user).firstname + ' ' + JSON.parse(localStorage.user).lastname :
   'Nama';
  const levelName = localStorage.user ?
  JSON.parse(localStorage.user).level_name :
  'Jabatan';

  return (
    <Fragment>
      <NavDesktop>
        <NavLink to="/" >
          <Brand bordered />
        </NavLink>
        <UserDetail>
          <div className="user-info">
            <h1>{ fullname }</h1>
            <p>{ levelName } - Cabang CIMONE</p>
          </div>
          <div className="buttons">
            <div className="profile">
              <img src={defaultProfile} alt="default"/>
            </div>
            <div className="notif">
              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bell" className="svg-inline--fa fa-bell fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                <path fill="#fff" d="M224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64zm215.39-149.71c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71z"></path>
              </svg>
            </div>
          </div>
        </UserDetail>
      </NavDesktop>
    </Fragment>
  )
}
const NavDesktop = styled.nav`
  @media ${device.tablet} {
    display: none;
  }
  position: fixed;
  width: 100%;
  height: ${sizes.navbarHeight};
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  padding: 0 36px;
  background-color: ${colors.primary};
  z-index: 1;
  .brand {
    width: 125px;
  }
`;
const UserDetail = styled.div`
  margin-left: auto;
  display: flex;
  .user-info {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-right: 1.8rem;
    color: white;
    text-align: right;
    h1 {
      font-size: 1.3rem;
      text-transform: uppercase;
    }
    p {
      font-size: 14px;
      color: rgba(255, 255, 255, .7);
      margin-top: .4rem;
    }
  }
  .buttons {
    display: flex;
    align-items: center;
    div:first-child {
      margin-right: 1rem;
    }
    .profile, .notif {
      background-color: ${colors.secondary};
      border-radius: 100px;
      height: 48px;
      width: 48px;
      padding: 8px;
      cursor: pointer;
      display: flex;
      justify-content: center;
      align-items: center;
      svg {
        width: 68%;
      }
    }
    .profile {
      padding: 0;
      overflow: hidden;
      img {
        width: 100%;
      }
    }
  }
`;

export default Navbar;
