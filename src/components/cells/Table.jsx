import React from 'react';
import styled from 'styled-components';

import { colors, device } from '../../pages/Layout';

export default ({ thead, tbody }) => {
  return (
    <Wrapper className="table-wrapper">
      <table>
        <thead>
          { thead }
        </thead>
        <tbody>
          { tbody }
        </tbody>
      </table>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  overflow-x: auto;
  box-shadow: 0 4px 18px rgba(0, 0, 0, .05);
  table {
    width: 100%;
    background-color: white;
    border-radius: 12px;
    overflow: hidden;
    th, td {
      padding: 15px;
      border: 1px solid #f0f0f0;
      ul {
        /* margin-bottom: .8rem; */
        &>li {
          display: flex;
          justify-content: center;
          margin-top: 4px;
        }
      }
      button {
        margin-top: auto;
        @media ${device.tablet} {
          font-size: 10px;
        }
      }
    }
    th {
      background-color: ${colors.primary};
      padding: 15px 0;
      color: white;
      font-size: 14px;
      @media ${device.tablet} {
        font-size: 12px;
      }
    }
    td {
      text-align: center;
      @media ${device.tablet} {
        font-size: 10px;
      }
      font-size: 12px;
      .buttons {
        button {
          @media ${device.tablet} {
            font-size: 10px;
          }
          &:first-child {
            margin-bottom: .5rem;
          }
        }
      }
      .status {
        @media ${device.tablet} {
          font-size: 10px;
        }
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
      }
    }
    tr {
      &:hover {
        background-color: rgba(0, 0, 0, .02);
      }
    }
  }
`;

