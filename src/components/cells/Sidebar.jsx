import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

import { sizes, colors, device } from "../../pages/Layout";

import beranda from "../../assets/img/png/beranda.png";
import logorgbordered from "../../assets/img/png/logo-rg-bordered.png";
import gadai from "../../assets/img/png/gadai.png";
import nasabah from "../../assets/img/png/nasabah.png";
import pengaturan from "../../assets/img/png/pengaturan.png";

const Sidebar = () => {
  return (
    <Aside>
      <div className="menu">
        <NavLink className="beranda" exact to="/">
          <div className="icon beranda-desktop">
            <img src={beranda} alt="" />
            <span>beranda</span>
          </div>
          <div className="icon beranda-mobile">
            <div>
              <img src={logorgbordered} alt="" />
            </div>
            <span>beranda</span>
          </div>
        </NavLink>
        <NavLink className="gadai" to="/barang">
          <div className="icon">
            <img src={gadai} alt="" />
            <span>barang</span>
          </div>
        </NavLink>
        <NavLink className="nasabah" to="/nasabah">
          <div className="icon">
            <img src={nasabah} alt="" />
            <span>terjual</span>
          </div>
        </NavLink>
        {/* <NavLink to="/pengaturan">
          <div className="icon">
            <img src={pengaturan} alt="" />
            <span>pengaturan</span>
          </div>
        </NavLink> */}
      </div>
    </Aside>
  );
};

const Aside = styled.aside`
  @media ${device.tablet} {
    position: fixed;
    left: 0;
    bottom: 0;
    top: unset;
    width: 100%;
    height: ${sizes.sidebarMobile};
    padding: 0;
    background-color: white;
    z-index: 5;
  }
  position: fixed;
  left: 0;
  top: 0;
  width: ${sizes.sidebarWidth};
  height: 100%;
  background-color: white;
  padding-top: ${sizes.navbarHeight};
  .menu {
    @media ${device.tablet} {
      display: flex;
      justify-content: space-around;
      height: 100%;
    }
    position: relative;
    a {
      text-decoration: none;
      @media ${device.tablet} {
        &.beranda {
          order: 2;
        }
        &.gadai {
          order: 1;
        }
        &.nasabah {
          order: 3;
        }
        &:last-child {
          display: none;
          order: 4;
        }
      }
      &.active {
        .icon {
          @media ${device.tablet} {
            background-color: transparent;
            &.beranda-mobile {
              div {
                background-color: ${colors.primary};
              }
            }
          }
          background-color: ${colors.white};
          div {
            opacity: 1;
          }
          img {
            opacity: 1;
          }
          span {
            opacity: 1;
          }
        }
      }
      &:hover {
        div {
          opacity: 1;
        }
        img {
          opacity: 1;
        }
        span {
          opacity: 1;
        }
      }
    }
    .icon {
      @media ${device.tablet} {
        width: ${sizes.sidebarMobile};
        height: 100%;
      }
      cursor: pointer;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      width: 100%;
      height: ${sizes.sidebarWidth};
      &.beranda-mobile {
        @media ${device.tablet} {
          margin-top: -20px;
          display: flex;
          position: relative;
          div {
            display: flex;
            width: 70px;
            height: 70px;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            opacity: 1;
            background-color: #e6d6eb;
            img {
              width: 50%;
            }
          }
          span {
            position: absolute;
            bottom: -14px;
          }
        }
        display: none;
      }
      &.beranda-desktop {
        @media ${device.tablet} {
          display: none;
        }
        display: flex;
      }
      img {
        @media ${device.tablet} {
          width: 50%;
        }
        width: 40%;
        margin-bottom: 0.3rem;
        opacity: 0.2;
        transition: 0.1s;
      }
      div {
        opacity: 0.2;
      }
      span {
        font-size: 14px;
        font-weight: 600;
        color: ${colors.primary};
        text-transform: capitalize;
        opacity: 0.2;
        transition: 0.1s;
      }
    }
  }
`;

export default Sidebar;
