import React, { useState, useEffect } from 'react';
import styled, { css } from 'styled-components';
import { FiX } from 'react-icons/fi';

import { colors, sizes, device } from '../../pages/Layout';
import Button from '../atoms/Button';

const heightHeader = 50;

export default ({
  children,
  width = '50%',
  headerLabel = 'Header Modal',
  cancelBtn = true,
  cancelLabel = 'Ngga',
  okeBtn = true,
  okeLabel = 'Iya',
  okeClick,
  footer = true,
  customFooter = '',
  showModal = false,
  toggleModal
}) => {

  return (
    <>
      <Modal width={width} isModal={showModal}>
        <section className="header">
          <div className="close"
            onClick={() => {
              toggleModal && toggleModal(false)
            }}
          >
            <FiX/>
          </div>
          <Wrapper>
            <center>
              <h5 className="h5">{headerLabel}</h5>
            </center>
          </Wrapper>
        </section>
        <section className="body">
          <Wrapper>
            {children}
          </Wrapper>
        </section>
        {footer && <section className="footer">
          <Wrapper>
            {!customFooter ? <center>
              {okeBtn && <Button oke
                onClick={okeClick && okeClick}
              >{okeLabel}</Button>}
              {cancelBtn && <Button cancel
                style={{
                  marginLeft: 8
                }}
                onClick={() => {
                  toggleModal(false);
                }}
              >{cancelLabel}</Button>}
            </center> : customFooter}
          </Wrapper>
        </section>}
      </Modal>
      <Bg isModal={showModal} />
    </>
  )
}

const Modal = styled.div`
  @media ${device.tablet} {
    margin: 20px 0 ${sizes.sidebarMobile} 0;
  }
  position: fixed;
  width: ${props => props.width};
  left: 50%;
  top: 50%;
  z-index: 4;
  transform: translate(-50%, -50%) scale(1);
  opacity: 1;
  background-color: white;
  border-radius: 12px;
  overflow: hidden;
  visibility: visible;
  transition: .2s;
  ${props => !props.isModal && css`
    visibility: hidden;
    transform: translate(-50%, -50%) scale(.6);
    opacity: 0;
  `};
  .header {
    position: relative;
    height: ${heightHeader}px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .close {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: white;
    height: ${heightHeader}px;
    width: ${heightHeader}px;
    top: 0;
    right: 0;
    cursor: pointer;
    z-index: 1;
    * {
      font-size: 1.5rem;
    }
  }
  .body {
    min-height: 36px;
    max-height: 70vh;
    overflow-Y: auto;
  }
  .footer {
    min-height: ${heightHeader}px;
    padding: 1rem 0;
  }
`;
const Bg = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0, 0, 0, .7);
  visibility: visible;
  z-index: 3;
  transition: .6s;
  ${props => !props.isModal && css`
    background-color: rgba(0, 0, 0, 0);
    visibility: hidden;
  `};
`;
const Wrapper = styled.div`
  padding: 0 1rem;
  width: 100%;
`;